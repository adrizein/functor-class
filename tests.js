'use strict';
/* eslint-disable no-new */

const assert = require('assert');

const {Functor, Mapper, Reducer, Filter, Composition} = require('./index');


describe('Functor', () => {
    describe('instantiation', () => {
        it('should return a callable object', () => {
            assert.doesNotThrow(
                () => {
                    const f = new Functor(() => 23);
                    f();
                },
                TypeError
            );
        });
    });

    describe('inheritance', () => {
        it('should return a class of function with attributes', () => {
            class Linear extends Functor {
                constructor(a) {
                    super((x) => this.a * x);
                    this.a = a;
                }
            }
            const f = new Linear(3);
            assert.strictEqual(f(2), 6);
        });
    });

    describe('invalid instantiation', () => {
        it('should throw a TypeError', () => assert.throws(() => new Functor('string'), TypeError));
    });
});


describe('Filter', () => {
    const array = [1, 2, 3, 4, 5, 6];
    function predicate(x) {return x > 3;}
    const result = [4, 5, 6];
    function *iter() {
        for (const i of array) {
            yield i;
        }
    }

    describe('instantiation', () => {
        it('should return a filtering functor', () => {
            assert.doesNotThrow(
                () => {
                    const f = new Filter(predicate);
                    f(array);
                },
                TypeError
            );
        });
    });

    describe('execution', () => {
        it('should filter an array', () => {
            const f = new Filter(predicate);
            assert.deepStrictEqual(f(array), result);
        });

        it('should filter an iterable', () => {
            const f = new Filter(predicate);
            assert.deepStrictEqual(f(iter()), result);
        });
    });

    describe('invalid instantiation', () => {
        it('should throw a TypeError', () => assert.throws(() => new Filter(34), TypeError));
    });
});


describe('Mapper', () => {
    const array = [1, 2, 3, 4, 5, 6];
    function mapper(x) {return x * x;}
    const result = [1, 4, 9, 16, 25, 36];
    function *iter() {
        for (const i of array) {
            yield i;
        }
    }

    describe('instantiation', () => {
        it('should return a mapping functor', () => {
            assert.doesNotThrow(
                () => {
                    const f = new Mapper(mapper);
                    f(array);
                },
                TypeError
            );
        });
    });

    describe('execution', () => {
        it('should map a function onto an array', () => {
            const f = new Mapper(mapper);
            assert.deepStrictEqual(f(array), result);
        });

        it('should map a function onto an iterable', () => {
            const f = new Mapper(mapper);
            assert.deepStrictEqual(f(iter()), result);
        });
    });

    describe('invalid instantiation', () => {
        it('should throw a TypeError', () => assert.throws(() => new Mapper(34), TypeError));
    });
});


describe('Reducer', () => {
    const array = [1, 2, 3, 4, 5, 6];
    function reducer(x, y) {return x * y;}
    const result = 720;
    function *iter() {
        for (const i of array) {
            yield i;
        }
    }

    describe('instantiation', () => {
        it('should return a reducing functor', () => {
            assert.doesNotThrow(() => {
                    const f = new Reducer(reducer);
                    f(array);
                },
                TypeError
            );
        });
    });

    describe('execution', () => {
        it('should reduce an array by a operator without an accumulator', () => {
            const f = new Reducer(reducer);
            assert.deepStrictEqual(f(array), result);
        });

        it('should reduce an array by a operator through an accumulator', () => {
            const f = new Reducer(reducer);
            assert.deepStrictEqual(f(array, 2), result * 2);
        });

        it('should reduce an iterator by a operator without an accumulator', () => {
            const f = new Reducer(reducer);
            assert.deepStrictEqual(f(iter()), result);
        });

        it('should reduce an iterator by a operator through an accumulator', () => {
            const f = new Reducer(reducer);
            assert.deepStrictEqual(f(iter(), 2), result * 2);
        });
    });

    describe('invalid instantiation', () => {
        it('should throw a TypeError', () => assert.throws(() => new Reducer(34), TypeError));
    });
});


describe('Composition', () => {
    const number = 3;
    const functions = [
        (x) => x + 3,
        (x) => [x, x * x],
        (x, y) => x + y,
    ];
    const result = 42;
    function *iter() {
        for (const i of functions) {
            yield i;
        }
    }

    describe('instantiation', () => {
        it('should return a functor', () => {
            assert.doesNotThrow(() => {
                    const f = new Composition(...functions);
                    f(number);
                },
                TypeError);
        });
    });

    describe('execution', () => {
        it('should compute the result of the recurrent application of an array of functions', () => {
            const f = new Composition(...functions);
            assert.deepStrictEqual(f(number), result);
        });

        it('should compute the result of the recurrent application of a sequence of functions', () => {
            const f = new Composition(...iter());
            assert.deepStrictEqual(f(number), result);
        });
    });

    describe('invalid instantiation', () => {
        it('should throw a TypeError', () => assert.throws(() => new Composition(34), TypeError));

        it(
            'should throw a TypeError when at least one argument is not a function',
            () => assert.throws(() => new Composition(...functions, 34), TypeError)
        );
    });
});
