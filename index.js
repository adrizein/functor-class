const _ = require('lodash');

class _Functor extends Function {

    constructor(f) {
        return Object.setPrototypeOf(f, new.target.prototype);
    }
}


class Functor extends _Functor {

    constructor(f, options) {
        super(f);
        // eslint-disable-next-line no-console
        this.options = _.merge({logger: console.log, debug: false}, options);
    }

    get logger() {
        return this.options.debug ? this.options.logger : _.noop;
    }

    set debug(debug) {
        this.options.debug = Boolean(debug);
    }

    set logger(logger) {
        // eslint-disable-next-line no-console
        this.options.logger = logger || console.log;
    }
}


class Filter extends Functor {

    constructor(predicate, options) {
        if (!_.isFunction(predicate)) {
            throw new TypeError(`The predicate of the filter should be a function, instead ${predicate} was passed.`);
        }
        super((array) => {
            const result = [];
            let i = 0;
            for (const element of array) {
                const keep = this.predicate(element);
                if (keep) {
                    this.logger(`element ${i++}: ${element} verifies the predicate`);
                    result.push(element);
                }
                else {
                    this.logger(`element ${i++}: ${element} does not verify the predicate`);
                }
            }
            this.logger(`Filtering [${array}] through ${this.predicate}`);
            if (array.length === 1) {
                this.logger(`${array.length} element before`);
            }
            else {
                this.logger(`${array.length} elements before`);
            }
            if (result.length === 1) {
                this.logger(`${result.length} element after`);
            }
            else {
                this.logger(`${result.length} elements after`);
            }
            this.logger(`Filtered array: [${result}]`);
            return result;

        }, options);
        this.predicate = predicate;
    }

    toString() {
        return `Filter(${this.predicate.toString()})`;
    }
}


class Reducer extends Functor {

    constructor(operator, options) {
        if (!_.isFunction(operator)) {
            throw new TypeError(`The reducing operator should be a function, instead ${operator} was passed.`);
        }

        super((iterable, accumulator) => {

            if (accumulator) {
                this.logger(`Reducing ${iterable} through ${this.operator} with ${accumulator} as the accumulator`);
                for (const element of iterable) {
                    this.logger(`accumulator = ${this.operator}(${accumulator}, ${element})`);
                    accumulator = this.operator(accumulator, element);
                    this.logger(`accumulator = ${accumulator}`);
                }
                this.logger(`Result: ${accumulator}`);
                return accumulator;
            }
            else {
                this.logger(`Reducing ${iterable} through ${this.operator}`);
                let result;
                for (const element of iterable) {
                    if (!result) {
                        result = result || element;
                        this.logger(`result = ${result}`);
                    }
                    else {
                        this.logger(`result = ${this.operator}(${result}, ${element})`);
                        result = this.operator(result, element);
                        this.logger(`result = ${result}`);
                    }
                }
                this.logger(`Result: ${result}`);
                return result;
            }
        },
        options);
        this.operator = operator;
    }

    toString() {
        return `Reducer(${this.operator.toString()})`;
    }
}

class Mapper extends Functor {

    constructor(func, options) {
        if (!_.isFunction(func)) {
            throw new TypeError(`The first argument should be a function, instead, ${func || 'nothing'} was passed`);
        }

        super((iterable) => {
            const result = [];
            this.logger(`Mapping ${this.func} on [${iterable}]`);
            let i = 0;
            for (const element of iterable) {
                const r = this.func(element);
                this.logger(`element ${i++}: ${this.func}(${element}) = ${r}`);
                result.push(r);
            }
            this.logger(`Result: [${result}]`);
            return result;
        },
        options);
        this.func = func;
    }
}


class Composition extends Functor {

    constructor(...functions) {
        if (!_.isArray(functions) || !functions.every(_.isFunction)) {
            throw new TypeError(`A composition takes a sequence of functions, instead ${functions} was passed.`);
        }

        super((...x) => {
            let r = x;
            let l = x;
            let i = 0;
            this.logger(`Input: ${r}`);
            for (const f of this.functions) {
                l = r;
                try {
                    r = f(...r);
                }
                catch (err) {
                    r = f(r);
                }
                this.logger(`Step ${i++}:  (${f.toString()})(${l.toString()}) = ${r.toString()}`);
            }
            this.logger(`Output after ${i} steps: ${r}`);
            return r;
        });
        this.functions = functions;
    }

    toString() {
        // TODO: show the actual expression (HARD)
        return `Composition(${this.functions.map((f) => f.toString()).toString()})`;
    }
}


module.exports = {
    Functor,
    Filter,
    Reducer,
    Mapper,
    Composition,
};
