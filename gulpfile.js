/* eslint-disable no-console */
const
    gulp = require('gulp'),
    path = require('path');

const $ = require('gulp-load-plugins')({
    pattern: ['gulp-*'],
});


gulp.task('lint', () => gulp.src([
    'index.js',
	'tests.js',
    'gulpfile.js',
    '!node_modules/**',
])
    .pipe($.eslint())
    .pipe($.eslint.format())
);


gulp.task('cov', () => gulp.src([
	'index.js',
])
    .pipe($.istanbul())
    .pipe($.istanbul.hookRequire())
    .on('error', handleError)
);


gulp.task('test', ['cov'], () => gulp.src([
    path.join('tests.js'),
])
    .pipe($.mocha())
    .pipe($.istanbul.writeReports())
    .on('error', handleError)
);


/////////////

function handleError(error) {
    console.error(error);

    this.emit('end');
}
